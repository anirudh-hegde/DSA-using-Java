package CycleSort;

public class MissingPositive {
    public static void main(String[] args) {
        int[] nums = {1,2,0};
        System.out.println(cycSort(nums));
    }

    static int cycSort(int[] nums) {
        int i = 0;
        while (i < nums.length) {
            if (nums[i] > 0 && nums[i] <= nums.length) {
                int correct = nums[i]-1;
                if (nums[i] != nums[correct]) {
                    swap(nums, correct, i);
                } else {
                    i++;
                }
            }
            for (int index = 0; index < nums.length; index++) {
                if (nums[index] != index+1) {
                    return index;
                }
            }
        }
        return nums.length;
    }

    public static void swap(int[] arr, int first, int second) {
        int temp = arr[first];
        arr[first] = arr[second];
        arr[second] = temp;

    }
}
//-------------------------------------------------------------------------------------------
//package CycleSort;
//
//public class MissingPositive {
//    public static void main(String[] args) {
//        int[] nums = {3, -1, 1};
//        System.out.println(cycSort(nums));
//    }
//
//    static int cycSort(int[] nums) {
//        int i = 0;
//        while (i < nums.length) {
//            if (nums[i] > 0 && nums[i] <= nums.length) { // check if nums[i] is in the valid range
//                int correct = nums[i] - 1; // correct index
//                if (nums[i] != nums[correct]) {
//                    swap(nums, correct, i);
//                } else {
//                    i++;
//                }
//            } else {
//                i++;
//            }
//        }
//
//        for (int index = 0; index < nums.length; index++) {
//            if (nums[index] != index + 1) { // indices are 0-based, so we add 1 for comparison
//                return index + 1; // the first missing positive integer
//            }
//        }
//
//        return nums.length + 1; // if no missing number in range, return the next number
//    }
//
//    static void swap(int[] arr, int first, int second) {
//        int temp = arr[first];
//        arr[first] = arr[second];
//        arr[second] = temp;
//    }
//}
