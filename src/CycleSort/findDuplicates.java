package CycleSort;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class findDuplicates {
    public static void main(String[] args) {
        int[] arr = {5, 6, 4, 1, 3, 1};
        System.out.println(duplicate(arr));
        System.out.println(Arrays.toString(arr));
    }

    static List<Integer> duplicate(int[] arr) {
        int i = 0;
        while (i < arr.length) {
            int correct = arr[i] - 1;
            if (arr[i] != arr[correct]) {
                swap(arr, correct, i);
            } else {
                i++;
            }
        }
        List<Integer> nums = new ArrayList<>();
        for (int index = 0; index < arr.length; index++) {
            if (arr[index] != index + 1) {
                nums.add(arr[index]);
            }
        }
        return nums;
    }

    static void swap(int[] arr, int first, int second) {
        int temp = arr[first];
        arr[first] = arr[second];
        arr[second] = temp;

    }
}
