package CycleSort;

import java.util.Arrays;

public class findDuplicate {
    public static void main(String[] args) {
        int[] arr = {4, 3, 1, 2, 2};

        System.out.println("duplicate element is : " + findduplicate(arr));
    }

    static int findduplicate(int[] arr) {

        int i = 0;
        while (i < arr.length) {
            if (arr[i] != i + 1) {
                int correct = arr[i] - 1;
                if (arr[correct] != arr[i]) {
                    swap(arr, correct, i);
                } else {
                    return arr[i];
                }
            } else {
                i++;
            }

        }
        return -1;
    }

    static void swap(int[] arr, int first, int second) {
        int temp = arr[first];
        arr[first] = arr[second];
        arr[second] = temp;
    }

}
