package com.arrays;
import java.util.Scanner;
public class ChangeVal {
    static void change(int[] arr) {
        arr[0] = 14;
    }
    static void ascii(){
        Scanner in=new Scanner(System.in);
        char val=in.next().charAt(0);

        System.out.println(val);
    }

    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5};
        change(arr);
        for (int i : arr) System.out.print(i + " ");
        System.out.println();
        ascii();
    }
}
