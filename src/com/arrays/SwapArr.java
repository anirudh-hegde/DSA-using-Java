package com.arrays;

//swapping the indices in an array
import java.util.Arrays;

public class SwapArr {
    public static void main(String[] args) {
        int[] arr={1,3,4,2,5,6};
        swap(arr,1,3);
    }
    public static void swap(int[] arr, int index1, int index2){
        int temp=arr[index1];
        arr[index1]=arr[index2];
        arr[index2]=temp;
        System.out.println(Arrays.toString(arr));
    }
}

