package com.arrays;

public class MinEle {
    public static void main(String[] args) {
        int[] arr={18,3,44,5,15};
        System.out.println(MinEle.min(arr));
    }
    static int min(int[] arr){
        int ans=arr[0];
        for(int i=0;i<arr.length;i++){
            if(arr[i]<ans)
                ans=arr[i];
        }
        return ans;
    }

}
