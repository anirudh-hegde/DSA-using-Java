package com.arrays;

import java.util.Arrays;

public class ReverseArr {
    public static void reverse(int[] arr) {
//        int index=arr.length-1;
//        while(index>=0){
//            System.out.println(arr[index]);
//            index--;
//        }
        int start = 0, end = arr.length - 1;
        while (start < end) {
            swap(arr, start, end);
            start++;
            end--;
        }
        System.out.println(Arrays.toString(arr));
    }

    public static void swap(int[] arr, int i1, int i2) {
        int temp = arr[i1];
        arr[i1] = arr[i2];
        arr[i2] = temp;
    }

    public static void main(String[] args) {
        int[] arr = {2, 1, 4, 3, 5};
        reverse(arr);
    }
}
