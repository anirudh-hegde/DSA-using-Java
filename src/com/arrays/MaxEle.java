package com.arrays;

//maxelement in an array
public class MaxEle {
    public static int maxEle(int[] arr) {
        int max = 0;
        for (int i : arr) {
            if (i > max) {
                max = i;
            }
        }
        return max;
    }

    public static void main(String[] args) {
        int[] arr = {1, 3, 23, 9, 18};
        System.out.println("the maximum element in an array: " + MaxEle.maxEle(arr));
    }
}
