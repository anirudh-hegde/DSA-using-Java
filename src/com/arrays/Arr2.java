package com.arrays;

import java.util.Scanner;

public class Arr2 {
    public void arrayAdd(int[] arr, int n) {
        int sum = 0;
        for (int i = 0; i < n; i++)
            sum += arr[i];
        System.out.println(sum);
//        return;
    }

    public static void main(String[] args){
        Arr2 arropt=new Arr2();
        Scanner in=new Scanner(System.in);
        int size=in.nextInt();
        int[] arr = new int[size];

//        int[] arr[size];
        for(int i=0;i<size;i++)
            arr[i]=in.nextInt();
        arropt.arrayAdd(arr,size);
//        System.out.println(arropt.arrayAdd(arr,size));

    }
}
