package com.arrays;

import java.util.Scanner;
import java.util.Arrays;

public class MultiDim {
    public static void main(String[] args) {
        Scanner in=new Scanner(System.in);
//        int size=4;
//        int[][] arr = {
//                {1, 2, 3},
//                {4, 5, 6},
//                {7, 8, 9}
//        };
//        for(int[] row:arr){
//            for(int ele:row){
//                System.out.print(ele+" ");
//            }
//            System.out.println();
//        }
//        System.out.print(Arrays.deepToString(arr));
        int[][] arr = new int[3][3];
        for (int row = 0; row < arr.length; row++) {
            for (int col = 0; col < arr[row].length; col++)
                arr[row][col] = in.nextInt();
        }
//        for (int row = 0; row < arr.length; row++) {
//            for (int col = 0; col < arr[row].length; col++)
//                System.out.print(arr[row][col]+" ");
//            System.out.println();
//
//        }
        for (int row = 0; row < arr.length; row++)
            System.out.println(Arrays.toString(arr[row]));
//        System.out.println(Arrays.deepToString(arr));
    }
}
