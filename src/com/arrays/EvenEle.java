package com.arrays;

import java.util.Arrays;

public class EvenEle {
    public static void main(String[] args) {
        int[] arr=new int[]{1,44,33,11,18};
        System.out.println("No. of even elements in "+ Arrays.toString(arr) +" is :"+EvenEle.even(arr));
        System.out.println("No. of odd elements are "+ Arrays.toString(arr) +" is :"+EvenEle.odd(arr));
    }
    static int even(int[] arr){
        int count=0;
        for(int i:arr){
            if(i%2==0)
                count++;
        }
        return count;
    }
    static int odd(int[] arr){
        int count1=0;
        for(int ii:arr){
            if(ii%2!=0)
                count1++;
        }
        return count1;
    }
}
