package com.arrays;

import java.util.ArrayList;

public class DynamicArray {
    public static void main(String[] args) {
        ArrayList<Integer> list= new ArrayList<>(5);
        int sum=0;
        int[][] arr={{1,2,3},{5,6,7}};
        for(int[] row:arr){
            for(int i:row){
                sum+=i;
                list.add(sum);
                
            }
        }
        System.out.println(list);
//        list.set(0,9);

//        System.out.println(list.get());
    }
}
