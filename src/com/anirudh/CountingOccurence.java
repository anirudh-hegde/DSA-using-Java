package com.anirudh;

import java.util.Scanner;

public class CountingOccurence {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int search = in.nextInt();
        int cnt = 0;
        while (n > 0) {
            int rem = n % 10;
            if (rem == search)
                cnt++;
            n = n / 10;
        }

        System.out.println(cnt);

    }

}
