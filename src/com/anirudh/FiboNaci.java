package com.anirudh;

import java.util.Scanner;

public class FiboNaci {
    public static void main(String[] args) {
        FiboNaci fb = new FiboNaci();
        Scanner in = new Scanner(System.in);
        int start = in.nextInt();
        int end = in.nextInt();
        fb.fibonacci(start, end);

    }

    public void fibonacci(int start, int end) {

        int a = 0, b = 1;
        while (a <= end) {
            if (a >= start)
                System.out.print(a + " ");
            int sum = a + b;
            a = b;
            b = sum;
        }
        System.out.println();
    }
}
