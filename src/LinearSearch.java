import java.util.Scanner;

public class LinearSearch {
    Scanner in = new Scanner(System.in);
    boolean found = false;
//program to write linearsearch algorithm for 1D array
    public void linearSearch1D() {
        int[] arr = {1, 2, 3, 4, 5};
        System.out.println("enter the element to be searched in 1D : ");
        int ele = in.nextInt();

        for (int i : arr) {
            if (ele == i) {
                found = true;
                break;
            }
        }
        if (found)
            System.out.println("element found");
        else
            System.out.println("element not found");
    }

    //program to write linearsearch algorithm for 1D array
    public void linearSearch2D(){
        int[][] arr1={{1,2,3},{4,5,6},{7,8,9}};
        System.out.println("enter the element to be searched in 2D : ");
        int n1=in.nextInt();
        in.close();
        for(int[] row:arr1){
            for(int j:row){
                if(n1==j){
                    found=true;
                    break;
                }
            }
        }
        if(found){
            System.out.println("element found in 2D ");
        }
        else{
            System.out.println("element not found in 2D");
        }
    }

    public static void main(String[] args) {
        LinearSearch ls=new LinearSearch();
        ls.linearSearch1D();
        System.out.println("--------------------------------------------------------");
        ls.linearSearch2D();
    }
}

