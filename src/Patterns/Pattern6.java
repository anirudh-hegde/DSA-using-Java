package Patterns;

public class Pattern6 {
    public static void main(String[] args) {
        pattern1(5);
        pattern2(5);
    }
    static void pattern1(int n){
        for(int row=1;row<=n;row++){
            for(int col=n;col>=row;col--){
                System.out.print(" ");
            }
            for(int col=1;col<=row;col++){
                System.out.print("* ");
            }
            System.out.println();
        }
    }
    static void pattern2(int n){
        int row,col;
        for(row=n-1;row>=1;row--){
            for(col=row;col<=n;col++){
                System.out.print(" ");
            }
            for(col=1;col<=row;col++){
                System.out.print("* ");
            }
            System.out.println();
        }
    }
}
