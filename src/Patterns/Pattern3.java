package Patterns;

public class Pattern3 {
    public static void main(String[] args) {
        pattern1(5);
        System.out.println("-----------------");
        pattern2(4);
    }

    static void pattern1(int n) {
        for (int row = 1; row <= n; row++) {
            for (int col = 1; col <= row; col++) {
                System.out.print(col + " ");
            }
            System.out.println();
        }

    }
    static void pattern2(int n){
        for(int row=n;row>=1;row--){
            for(int col=1;col<=row;col++){
                System.out.print(col + " ");
            }
            System.out.println();
        }
    }
}