package Patterns;

public class Pattern1 {
    public static void main(String[] args) {
        pattern1(5);
        System.out.println();
//        pattern2(5);
        pattern2(5);
    }
    static void pattern1(int n){
        for(int row=1;row<=n;row++){
            for(int col=1;col<=row;col++){
                System.out.print("* ");
            }
            System.out.println();
        }
    }
    static void pattern2(int n){
        for(int row=n;row>=1;row--){
            for(int space=row;space<=n;space++){
                System.out.print("  ");
            }
            for(int col=1;col<=row;col++){
                System.out.print("* ");
            }
            System.out.println();
        }
    }

}
