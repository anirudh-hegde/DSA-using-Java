package Patterns;

public class Pattern8 {
    static int row, col, space;
    public static void main(String[] args) {
        pattern1(4);
        System.out.println("--------------------------------------");
        pattern2(4);
    }

    static void pattern1(int n) {

        for (row = 1; row <= n; row++) {
            for (col = 1; col <= row; col++) {
                System.out.print("*");
            }
            for (space = row; space < n; space++) {
                System.out.print("  ");
            }
            for (col = 1; col <= row; col++) {
                System.out.print("*");
            }
            System.out.println();
        }
    }
    static void pattern2(int n){
        for(row=n;row>=1;row--){
            for(col=1;col<=row;col++){
                System.out.print("*");
            }
            for(space=row+1;space<=n;space++){
                System.out.print("  ");
            }
            for(col=1;col<=row;col++){
                System.out.print("*");
            }
            System.out.println();
        }
    }
}
