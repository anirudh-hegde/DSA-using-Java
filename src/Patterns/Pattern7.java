package Patterns;

public class Pattern7 {
    static int row,col,space;

    public static void main(String[] args) {
        pattern1(4);
        System.out.println();
        pattern2(4);
    }
    static void pattern1(int n){
        for(row=1;row<=n;row++){
            for(space=n;space>=row;space--){
                System.out.print("  ");
            }
            for(col=row;col>=1;col--){
                System.out.print(col+" ");
            }
            for(col=2;col<=row;col++){
                System.out.print(col+" ");
            }
            System.out.println();
        }
    }
    static void pattern2(int n) {
        for (row = n; row >=1; row--){
            for(space=row;space<=n;space++){
                System.out.print("  ");
            }
            for(col=row;col>=1;col--){
                System.out.print(col+" ");
            }
            for(col=2;col<=row;col++){
                System.out.print(col+" ");
            }
            System.out.println();
        }
    }
}
