package Binarysearches;

public class Infinite_array {
    public static void main(String[] args) {
        int[] arr={1, 2, 4, 6, 8, 9, 12, 14, 17, 21, 45};
        int target=9;
        System.out.println(binarySearch(arr,target));
    }
//    static int ans(int[] arr,int target){
////        first start with a box of size 2
//
//    }
    static int binarySearch(int[] arr, int target) {
        int start=0;
        int end=1;
        while(target>arr[end]){
            start=end+1;
            end=2*end;
        }
        while (start <= end) {
            int mid = start + (end - start) / 2;
            if (target < arr[mid]) {
                end = mid - 1;
            } else if (target > arr[mid]) {
                start = mid + 1;
            } else {
                return mid;
            }
        }
        return -1;
    }
}
