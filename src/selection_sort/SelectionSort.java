package selection_sort;

import java.util.Arrays;

public class SelectionSort {
    public static void main(String[] args) {
        int[] arr={5,4,6,1,2};
        selection(arr);
        System.out.println(Arrays.toString(arr));
    }
    static void selection(int[] arr){
        for(int i=0;i< arr.length;i++){
            int end=arr.length-i-1;
            int maxele=getMaxEle(arr,0,end);
            swap(arr,maxele,end);
        }
    }
    static void swap(int[] arr,int start,int second){
        int temp=arr[start];
        arr[start]=arr[second];
        arr[second]=temp;
    }
    static int getMaxEle(int[] arr,int start,int end){
        int max=start;
        for(int i=start;i<=end;i++){
            if(arr[i]>arr[max])
                max=i;
        }
        return max;
    }
}
