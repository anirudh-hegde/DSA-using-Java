package Recursion;

public class binsrch_with_recurse {
    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5};
        int search = 5;
        System.out.println(binarySearch(arr, 0, search, arr.length - 1));
    }

    static int binarySearch(int[] arr, int start, int search, int end) {
        if (start > end) {
            return -1;
        }
        int mid = start + (end - start) / 2;
        if (arr[mid] > search) {
            return binarySearch(arr, start, search, mid - 1);
        } else if (arr[mid] < search) {
            return binarySearch(arr, mid + 1, search, end);
        } else return mid;

    }
}
