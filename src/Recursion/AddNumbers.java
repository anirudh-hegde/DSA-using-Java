package Recursion;

public class AddNumbers {
    public static void main(String[] args) {
        System.out.println("sum is "+sum(1));
    }
    static int sum(int n){
        if(n>5)
            return 0;
        return n+sum(n+1);
    }
}
