package Recursion;

public class FindFactorial {
    public static void main(String[] args) {
        System.out.println("factorial is:"+findFactorial(5));
    }
    static int findFactorial(int n){
        if(n==0)
            return 1;
        return n*findFactorial(n-1);
    }
}
